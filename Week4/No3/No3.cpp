#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <sstream>
#include <list>
#include <map>
#include <queue>
#include <algorithm>

using namespace std;


vector<pair<char, char>> edge;
map<char, bool> visited;

bool isCyclic(char node){
	for(int i = 0; i < edge.size(); i++){
		if(edge[i].first == node){
			if(visited[edge[i].second])
				return true;
		}
	}

	return false;
}

bool dfs(const vector<char> Node, string cp){
	if(cp.length() == Node.size()){
		cout << cp[0];
		for(int i = 1; i < cp.size(); i++)
			cout << " " << cp[i];
		cout << endl;
		return true;
	}

	bool hasAns = false;
	for(int i = 0; i < Node.size(); i++){
		if(!visited[Node[i]] && !isCyclic(Node[i])){
			visited[Node[i]] = true;
			hasAns = dfs(Node, cp + Node[i]) || hasAns;
			visited[Node[i]] = false;
		}

		
	}

	return hasAns;
}

int main(void){
	int Cases;
	string s;
	vector<char> Node;
	
	cin >> Cases;
	cin.ignore();

	while(Cases--){

		for(int i = 0; i < 26; i++)
			visited.insert({'A' + i, false});
		
		cin.ignore();
		getline(cin, s);
		istringstream iss(s);

		char c;
		while(iss >> c)
			Node.push_back(c);

		getline(cin, s);
		iss.str("");
		iss.clear();
		iss.str(s);
		char from, to;
		while(iss >>from>> c>>to)
			edge.push_back({from, to});

		sort(Node.begin(), Node.end());
		if(!dfs(Node, ""))
			cout << "NO" << endl;
		if(Cases != 0) cout << endl;

		Node.clear();
		visited.clear();
		edge.clear();
	}

	return 0;
}