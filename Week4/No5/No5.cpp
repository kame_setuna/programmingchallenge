#include <iostream>
#include <vector>
#include <utility>
#include <set>
#include <algorithm>
#include <iterator>
#include <cstring>

using namespace std;


bool mycom(const pair<int, int> & x, const pair<int, int> & y){
	if(x.second == y.second)
		return x.first < y.first;
	return x.second > y.second;
}


class locateBestPosition{
public:
	vector<vector<int>> Edge;
	int nChild,  cnt;
	int Visited[10001];
	int num[10001], low[10001], parent[10001], Value[10001];

public:
	locateBestPosition(){};
	locateBestPosition(int N) : Edge(N+1), nChild(0), cnt(0){
		memset(Visited, 0, sizeof(Visited));
		memset(Value, 0, sizeof(Value));
	}

	void VisitConnectedStaion(int StationNumber, const int TargetStaion){
		if(Visited[StationNumber] == 1 || StationNumber == TargetStaion) return;

		Visited[StationNumber] = 1;
		for(auto it = Edge[StationNumber].begin(); it != Edge[StationNumber].end(); it++){
			if(Visited[*it] == 0)
				VisitConnectedStaion(*it, TargetStaion);
		}
	}

	void CalculateValue(const int TargetStation ,const int root){
		num[TargetStation] = low[TargetStation] = cnt++;
		Visited[TargetStation] = 1;

		for(int i = 0; i < Edge[TargetStation].size(); i++){
			int v = Edge[TargetStation][i];
			if(Visited[v] == 0){
				parent[v] = TargetStation;
				if(TargetStation == root) nChild++;

				
				CalculateValue(v, root);
				if(low[v] >= num[TargetStation])
					Value[TargetStation]++;
				low[TargetStation] = min(low[TargetStation], low[v]);
			} else if(v != parent[TargetStation]){
				low[TargetStation] = min(low[TargetStation], num[v]);
			}
		}
	}
};




int main(void){
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int N, M;
	int x, y;
	
	//Be aware that staion number is from 1 to N
	// NOT 0 to N-1

	while(cin >> N >> M, N){
		locateBestPosition Station(N);
		vector<pair<int, int>> ans;
		int cc = 0;

		while(cin >> x >> y, x != -1){
			Station.Edge[x+1].push_back(y+1);
			Station.Edge[y+1].push_back(x+1);
		}

		for(int i = 1; i <= N; i++){
			if(Station.Visited[i] == 0){
				Station.cnt = 0;
				Station.nChild = 0;
				Station.CalculateValue(i, i);
				Station.Value[i] = Station.nChild - 1;
				cc++;
			}
			ans.push_back({i, Station.Value[i]});
		}

		sort(ans.begin(), ans.end(), mycom);
		vector<pair<int, int>>::iterator pair_it = ans.begin();
		for(int i = 0; i < M; i++ ,pair_it++)
			cout << (*pair_it).first-1 << " " << (*pair_it).second + cc<< endl;
		cout << endl;

	}



	return 0;
}