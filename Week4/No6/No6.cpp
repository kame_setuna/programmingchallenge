#include <iostream>
#include <vector>

using namespace std;

vector<int> intersections[2001];
bool Visited[2001];

void VisitIntersectionsConnected(int i){
        if(Visited[i] == true) return;

        Visited[i] = true;
        auto it = intersections[i].begin();
        for(; it != intersections[i].end(); it++){
                if(Visited[*it] == false)
                        VisitIntersectionsConnected(*it);
        }
}

int main(void) {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int N, M;
	

        while (cin >> N >> M, N) {
                for (int i = 1; i <= N; i++) 
                        intersections[i].clear();
                

                int V,W,P;
                for (int i = 0; i < M; i++) {
                        cin >> V >> W >> P;

                        if(P == 1)
                                intersections[V].push_back(W);
                        else if(P == 2){
                                intersections[V].push_back(W);
                                intersections[W].push_back(V);
                        }
                }

		Visited[0] = true;
		for(int i = 1; i <= N; i++){
			for(int j = 1; j <= N; j++)Visited[j] = false;
			VisitIntersectionsConnected(i);

			
			for(int j = 0; j <= N; j++){
				if(Visited[j] == false){
					Visited[0] = false;
					cout << 0 << endl;
					i = N;
					break;
				}
			}

			
		}

		if(Visited[0])
			cout << 1 << endl;

        }

	return 0;
}