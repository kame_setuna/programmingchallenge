#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstring>
#include <set>
#include <string>

using namespace std;

int cnt;
int field[202][202];
// 0: white pixel
// 1: black pixel
// 2: visited
// -1: not used

void Visited0Area(int i, int j){
	if(field[i][j] == -1 || field[i][j] == 1 || field[i][j] == 2) return;

	field[i][j] = 2;

	Visited0Area(i - 1, j);
	Visited0Area(i, j - 1);
	Visited0Area(i, j + 1);
	Visited0Area(i + 1, j);
}

void CountWhiteZone(int i, int j){
	if(field[i][j] == -1 || field[i][j] == 2) return;

	if(field[i][j] == 0){
		Visited0Area(i, j);
		cnt++;
		return;
	}

	field[i][j] = 2;

	CountWhiteZone(i - 1, j);
	CountWhiteZone(i, j - 1);
	CountWhiteZone(i + 1, j);
	CountWhiteZone(i, j + 1);

	return;
}

int main(void){
	int H, W;
	int i_tmp;
	int caseCnt = 1;
	char c_tmp;
	multiset<string> ans;

	while(cin >> H >> W, H){
		memset(field, -1, sizeof(field));
		for(int i = 1; i <= H; i++){
			for(int j = 1; j <= W * 4; j++){
				cin >> c_tmp;
				sscanf(&c_tmp, "%x", &i_tmp);
				field[i][j + 3] = i_tmp % 2, i_tmp /= 2;
				field[i][j + 2] = i_tmp % 2, i_tmp /= 2;
				field[i][j + 1] = i_tmp % 2, i_tmp /= 2;
				field[i][j] = i_tmp % 2, i_tmp /= 2;
				j += 3;
			}
		}

		for(int i = 1; i <= H; i++){
			if(field[i][1] == 0)
				Visited0Area(i, 1);
			if(field[i][W * 4] == 0)
				Visited0Area(i, W * 4);
		}
		for(int j = 1; j <= W * 4; j++){
			if(field[1][j] == 0)
				Visited0Area(1, j);
			if(field[H][j] == 0)
				Visited0Area(H , j);
		}

		cout << "Case " << caseCnt++ << ": ";

		for(int i = 1; i <= H; i++){
			for(int j = 1; j <= 4 * W; j++){
				if(field[i][j] == 1){
					cnt = 0;
					CountWhiteZone(i, j);
					switch(cnt){
						case 0:
							ans.insert("W");
							break;
						case 1:
							ans.insert("A");
							break;
						case 2:
							ans.insert("K");
							break;
						case 3:
							ans.insert("J");
							break;
						case 4:
							ans.insert("S");
							break;
						case 5:
							ans.insert("D");
							break;
					}
				}
			}
		}

		for(auto it = ans.begin(); it != ans.end(); it++)
			cout << it->c_str();
		cout << endl;
		ans.clear();
	}

	return 0;
}