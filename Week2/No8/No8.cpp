#include <iostream>
#include <string>
#include <set>
#include <functional>

using namespace std;

class Jeep{
	int m_FuelConsumption;
	double m_ValueOfSmallestTank;
	int m_FuelLeak;
	int m_distanceFromStart;
	set<double, greater<double>> Answers;

public:
	Jeep() : m_FuelConsumption(0), m_FuelLeak(0),
		m_ValueOfSmallestTank(0), m_distanceFromStart(0){
	}

	void Initializing(void){
		m_FuelConsumption = 0;
		m_FuelLeak = 0;
		m_ValueOfSmallestTank = 0.0;
		m_distanceFromStart = 0;
		Answers.clear();
	}

	double CalculatingFuelConsumption(int d){
		double C1, C2;

		C1 = (double)m_FuelConsumption * (d - m_distanceFromStart) / 100.0;
		C2 = (double)m_FuelLeak * (d - m_distanceFromStart);

		return C1 + C2;
	}

	void UpdatingFuelConsumption(int d, int value){
		

		m_ValueOfSmallestTank += CalculatingFuelConsumption(d);
		m_distanceFromStart = d;
		m_FuelConsumption = value;
	}

	void ArrivingAtGoal(int d){
		
		m_ValueOfSmallestTank += CalculatingFuelConsumption(d);
		
		Answers.insert(m_ValueOfSmallestTank);
		printf("%.03lf\n", *(Answers.begin()));
	}

	void TankPunctured(int d){
		m_ValueOfSmallestTank += CalculatingFuelConsumption(d);

		m_distanceFromStart = d;
		m_FuelLeak++;
	}

	void GasStation(int d){

		m_ValueOfSmallestTank += CalculatingFuelConsumption(d);

		m_distanceFromStart = d;
		Answers.insert(m_ValueOfSmallestTank);
		m_ValueOfSmallestTank = 0;
	}

	void Mechanic(int d){
		m_ValueOfSmallestTank += CalculatingFuelConsumption(d);

		m_distanceFromStart = d;
		m_FuelLeak = 0;
	}
};

int main(void){
	int dist, Value;
	string eventName, tmp;
	Jeep jeep;

	while(1){
		cin >> dist;
		cin >> eventName;

		if(eventName == "Fuel"){
			cin >> tmp >> Value;

			if(dist == 0 && Value == 0)
				break;

			jeep.UpdatingFuelConsumption(dist, Value);
		} else if(eventName == "Gas"){
			cin >> tmp;
			jeep.GasStation(dist);
		} else if(eventName == "Leak"){
			jeep.TankPunctured(dist);
		} else if(eventName == "Mechanic"){
			jeep.Mechanic(dist);
		} else{
			jeep.ArrivingAtGoal(dist);
			jeep.Initializing();
		}

		
	}

	return 0;
}
