#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

int field[1025][1025];

pair<pair<int, int>, int> findMaxPoint(int field[][1025], const int d){
	pair<pair<int, int>, int> max;
	max.second = 0;

	for(int i = 0; i < 1025; i++)
		for(int j = 0; j < 1025; j++){
			if(field[i][j] > max.second){
				max.second = field[i][j];
				max.first.first = i;
				max.first.second = j;
			}
		}
	return max;
}

int main(void){
	int scenarios, d, n;
	pair<pair<int, int>, int> coordinate;

	cin >> scenarios;

	while(scenarios--){
		for(int i = 0; i < 1025; i++)
			for(int j = 0; j < 1025; j++)
				field[i][j] = 0;

		cin >> d >> n;
		int x, y, p;
		while(n--){
			cin >> x >> y >> p;

			for(int i = max(0, x - d); i <= min(1024, x + d); i++)
				for(int j = max(0, y - d); j <= min(1024, y + d); j++){
					field[i][j] += p;
				}
		}

		coordinate = findMaxPoint(field, d);
		cout << coordinate.first.first << " "
			<< coordinate.first.second << " "
			<< coordinate.second << endl;
	}

	return 0;
}