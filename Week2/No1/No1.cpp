#include <iostream>
#include <set>

using namespace std;

int main(void){
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);

	int n, m, tmp;

	while(cin >> n >> m){
		if(n == 0) return 0;

		multiset<int> dragon, knights;
		while(n--){
			cin >> tmp;
			dragon.insert(tmp);
		}

		while(m--){
			cin >> tmp;
			knights.insert(tmp);
		}

		int cost = 0;

		multiset<int>::iterator itK = knights.begin(), itD = dragon.begin();

		for(; itK != knights.end(); itK++){
			if(itD == dragon.end())	break;
			if(*itK >= *itD){
				cost += *itK;
				itD = dragon.erase(itD);
			}
		}

		if(itD != dragon.end()){
			cout << "Loowater is doomed!" << endl;
		} else
			cout << cost << endl;
	}
}
