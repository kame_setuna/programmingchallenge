#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;
int search_saetNo(vector<int> &seat, int a){
	for(int i = 0; i < seat.size(); i++){
		if(seat[i] == a) return i;
	}
}

int factorial(int x){
	int sum = 1;
	for(int i = 1; i <= x; i++){
		sum *= i;
	}
	return sum;
}

int Solve(vector<int> &seat, vector<pair<pair<int, int>, int>> &c){
	int cnt = 0;
	int a, b, d;

	if(c.empty())
		return factorial(seat.size());

	do{
		auto it = c.begin();
		for(; it != c.end(); it++){
			a = search_saetNo(seat, (*it).first.first);
			b = search_saetNo(seat, (*it).first.second);
			d = abs(a - b);

			if((*it).second > 0){
				if((*it).second < d)
					break;
			} else{
				if(-(*it).second > d)
					break;
			}
		}
		if(it == c.end()) cnt++;
	} while(next_permutation(seat.begin(), seat.end()));

	return cnt;
}

int main(void){
	vector<pair<pair<int, int>, int>> constraint;
	int n, m, c, a, b;
	vector<int> seat;

	while(cin >> n >> m, n){
		for(int i = 0; i < n; i++) seat.push_back(i);
		while(m--){
			cin >> a >> b >> c;
			if(a > b) swap(a, b);
			constraint.push_back({{a,b},c});
		}

		cout << Solve(seat, constraint) << endl;
		seat.clear();
		constraint.clear();
	}

	return 0;
}
