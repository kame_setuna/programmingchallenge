#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool immediatelydecodable(const vector<string>& codes){
	for(auto i = codes.begin(); i != codes.end() - 1; i++){
		for(auto j = i + 1; j != codes.end(); j++){
			string s = (*i).substr(0, (*j).length());
			if(s == *j) return false;
			s = (*j).substr(0, (*i).length());
			if(s == *i) return false;
		}
	}

	return true;
}

int main(void){

	int set = 1;
	string tmp;

	while(cin >> tmp){
		vector<string> codes;
		do{
			codes.push_back(tmp);
		} while(cin >> tmp, tmp != "9");

		if(immediatelydecodable(codes) == true)
			cout << "Set " << set++ << " is immediately decodable" << endl;
		else
			cout << "Set " << set++ << " is not immediately decodable" << endl;


	}
}
