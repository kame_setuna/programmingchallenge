#include <iostream>
#include <vector>

using namespace std;

unsigned long long  numOfelementsOftheset[1000];

unsigned long long Calnum(int s, int e){
	int i = s;
	unsigned long long ans = 0;
	int a = e - s;
	if(numOfelementsOftheset[a] != 0) 
		return numOfelementsOftheset[a];
	if(e < s || s > e) return 1;
	if(e - s == 0) return 1;
	if(e - s == 1) return 2;

	for(; i <= e; i++){
		int left, right;
		left = Calnum(s, i-1);
		right = Calnum(i+1, e);
		ans += left * right;
	}

	return numOfelementsOftheset[a] = ans;
}

int main(void){
	int a;

	while(cin >> a){
		for(int i = 0; i < a; i++) numOfelementsOftheset[i] = 0;
		cout << Calnum(0, a - 1) << endl;

	}

	return 0;
	
}