#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

typedef struct{
	int from;
	int to;
	int cost;
}Edges;

Edges edges[2000]; 

bool BellmanFord(int s, int v, int e){
//true: negative loop exists
//false: negative loop dosent exist
	vector<int> dist(v, INT_MAX -3000);
	dist[s] = 0;

	for(int i = 0; i < v; i++){
		for(int j = 0; j < e; j++){
			if(dist[edges[j].from] + edges[j].cost < dist[edges[j].to]){
				dist[edges[j].to] = dist[edges[j].from] + edges[j].cost;
				if(i == v - 1) return true;
			}
		}		
	}

	return false;
}


int main(void){
	int c, n, m;

	cin >> c;

	while(c--){
		cin >> n >> m;
		int from, to, cost;

		for(int i = 0; i < m; i++){
			cin >> from >> to >> cost;
			edges[i].from = from;
			edges[i].to = to;
			edges[i].cost = cost;
		}

		if(BellmanFord(0, n, m))
			cout << "possible" << endl;
		else
			cout << "not possible" << endl;
	}


}