#include <iostream>
#include <string>
#include <queue>
#include <algorithm>
#include <vector>
#include <string.h>

using namespace std;

typedef pair<int, int > pairInt;

const int INF = 10000;
int dx[] = {0, 0, 1, -1, -1, 1, -1, 1}, dy[] = {1,-1, 0, 0, 1, 1, -1, -1};

string map[51];
int h, w;
int hours[50][50];
int treasures;

void BFS(int sx, int sy){
	for(int i = 0; i < h; i++)
		for(int j = 0; j < w; j++)
			hours[i][j] = INF;

	if(map[sx][sy] == '~') return;

	queue<pairInt> Q;
	Q.push({sx, sy});
	hours[sx][sy] = 0;

	int tresuresFound = 0;
	while(!Q.empty()){
		pairInt p = Q.front(); Q.pop();

		for(int i = 0; i < 4; i++){
			int nx = p.first + dx[i], ny = p.second + dy[i];

			//if(map[nx][ny] == '!') tresuresFound++;
			//if(treasures == tresuresFound) break;
			if(0 <= nx && nx < h && 0 <= ny && ny < w &&
			   map[nx][ny] != '~' && map[nx][ny] != '#' && hours[nx][ny] == INF){
				Q.push({nx,ny});
				hours[nx][ny] = hours[p.first][p.second] + 1;
			}
		}
	}
}

int dp[11][1 << 11];
int cost[11][11];
int start = 0;

int visit_(int p, int v, int n){
	if(v == (1 << n) - 1) return cost[p][start];
	if(dp[p][v] != -1) return dp[p][v];

	int tmp = INF;
	for(int i = 0; i < n; i++)
		if((v & (1 << i)) == 0)
			tmp = min(tmp, cost[p][i] + visit_(i, v | (1 << i), n));
	dp[p][v] = tmp;

	return tmp;
}

int visit(int p, int v, int n){
	memset(dp, -1, sizeof(dp));
	start = 0;

	return visit_(p, v, n);
}

int main(void){
	int land_x, land_y;

	while(cin >> h >> w, h){
		vector<pair<int, pairInt>> treasurePoint;

		for(int i = 0; i < h; i++)
			cin >> map[i];

		for(int i = 0; i < h; i++){
			for(int j = 0; j < w; j++){
				if(map[i][j] == '!'){
					int tmp = treasurePoint.size() + 1;
					treasurePoint.push_back({tmp, {i,j}});
				}
				if(map[i][j] == '@')
					land_x = i, land_y = j;
			}
		}

		for(int i = 0; i < h; i++){
			for(int j = 0; j < w; j++){
				if(map[i][j] == '*'){
					int nx, ny;
					for(int k = 0; k < 8; k++){
						nx = i + dx[k], ny = j + dy[k];
						if(0 <= nx && nx < h && 0 <= ny && ny < w){
							if(map[nx][ny] == '.' || map[nx][ny] == '!' ||
							   map[nx][ny] == '@'){
								map[nx][ny] = '~';
							}
						}
					}
				}
			}
		}

		treasurePoint.insert(treasurePoint.begin(), {0, { land_x, land_y}});
		for(int i = 0; i < treasurePoint.size(); i++){
			int x, y;
			x = treasurePoint[i].second.first, y = treasurePoint[i].second.second;
			treasures = treasurePoint.size();
			BFS(x, y);

			for(int j = 0; j < treasurePoint.size(); j++){
				x = treasurePoint[j].second.first;
				y = treasurePoint[j].second.second;
				cost[i][j] = hours[x][y];
			}
		}

		int ans = visit(0, 0, treasurePoint.size());
		if(ans == INF)
			cout << -1 << endl;
		else
			cout << ans << endl;
	}
}