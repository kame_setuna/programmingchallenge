#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(void) {
	int N, K, M;
	char c;
	int v;
	

	cin >> N;

	while(N--) {
		int sum = 0;
		map<char, int> value;
		cin >> K;
		while(K--) {
			cin >> c >> v; 
			value.insert({ c,v });
		}

		cin >> M;
		cin.ignore();
		string s;
		while(M--) {
			
			getline(cin, s);

			for(auto it = s.begin(); it != s.end(); it++) {
				map<char, int>::iterator tmpIt = value.find(*it);
				if(tmpIt != value.end()) {
					sum += (*tmpIt).second;
				}
			}
		}

		printf("%d.%02d$\n", sum / 100, sum % 100);
	}

}
