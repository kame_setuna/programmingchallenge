#include <iostream>
#include <set>
#include <vector>

using namespace std;
int main(void) {
	int cnt;
	int num;
	set<int> Jolly;
	vector<int> order;

	while(cin >> cnt) {
		Jolly.clear();
		order.clear();
		for(int i=0;i<cnt;i++) {
			cin >> num;
			order.push_back(num);
		}
		
		if(cnt == 1) {
			cout << "Jolly" << endl;
			continue;
		}

		for(int i = 1; i < cnt; i++)
			Jolly.insert(i);

		
		for(auto it = order.begin(); it != order.end() - 1; it++)
			Jolly.erase(abs(*(it + 1) - *(it)));

		Jolly.size() == 0 ? puts("Jolly") : puts("Not jolly");
	}

	return 0;
}
