#include <iostream>

using namespace std;

int main(void) {

	int leftS[100002], rightS[100002];

	int S, B;
	
	while(cin >> S >> B) {
		if(S == 0 && B == 0)
			break;
		for(int i = 1; i <= S; i++) {
			leftS[i] = i - 1;
			rightS[i] = i + 1;
		}

		leftS[1] = -1;
		rightS[S] = -1;

		while(B--) {
			int L, R;
			cin >> L >> R;

			leftS[rightS[R]] = leftS[L];
			rightS[leftS[L]] = rightS[R];

			if(leftS[L] != -1)
				cout << leftS[L] << " ";
			else
				cout << "* ";

			if(rightS[R] != -1)
				cout << rightS[R] << endl;
			else
				cout << "*" << endl;

		}

		puts("-");
	}

	return 0;
}
