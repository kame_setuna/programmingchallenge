#include <iostream>
#include <set>

using namespace std;
int main(void) {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);

	int N, M, cat;
	set<int> Jack;

	while(cin >> N >> M) {
		if(N == 0 && M == 0) break;

		
		while(N--) {
			cin >> cat;
			Jack.insert(cat);
		}

		auto endIt = Jack.end();
		int cnt = 0;
		while(M--) {
			cin >> cat;
			if(Jack.find(cat) != endIt)
				cnt++;
		}
		cout << cnt << endl;
		Jack.clear();
	}


	return 0;
}
