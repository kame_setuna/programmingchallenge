#include <iostream>
#include <vector>

using namespace std;

class UnionFind {
	vector<int> par;
	vector<int> sizes;
	const int N;
public:
	UnionFind(int n) : N(n), par(2 * n), sizes(2 * n) {
		for(int i = 0; i < 2 * N; i++) {
			par[i] = i;
			sizes[i] = 1;
		}
	}

	int GetRoot(const int x) {
		if(x == par[x]) return x;
		return par[x] = GetRoot(par[x]);
	}

	void joint(int x, int y) {
		x = GetRoot(x);
		y = GetRoot(y);

		if(x == y) return;

		if(sizes[x] < sizes[y]) {
			par[x] = y;
			sizes[y] += sizes[x];
		} else {
			par[y] = x;
			sizes[x] += sizes[y];
		}
	}

	bool isSame(int x, int y) {
		return GetRoot(x) == GetRoot(y);
	}

	int enemyID(int x) {
		return x + N;
	}

	void setFriends(int x, int y) {
		if(areEnemies(x, y)) {
			cout << "-1" << endl;
			return;
		}

		joint(x, y);
		joint(enemyID(x), enemyID(y));
	}

	void setEnemies(int x, int y) {
		if(areFriends(x, y)) {
			cout << "-1" << endl;
			return;
		}

		joint(x, enemyID(y));
		joint(y, enemyID(x));
	}

	bool areFriends(int x, int y) {
		return isSame(x, y) || isSame(enemyID(x), enemyID(y));
	}

	bool areEnemies(int x, int y) {
		return isSame(x, enemyID(y)) || isSame(y, enemyID(x));
	}
};

int main(void) {
	int N;

	cin >> N;

	UnionFind Relations(N);

	int c, x, y;

	while(cin >> c >> x >> y) {
		if(c == 0)return 0;

		switch(c) {
			case 1:
				if(Relations.areEnemies(x, y)) {
					cout << "-1" << endl;
					break;
				}
				Relations.setFriends(x, y);
				break;
			case 2:
				if(Relations.areFriends(x, y)) {
					cout << "-1" << endl;
					break;
				}
				Relations.setEnemies(x, y);
				break;
			case 3:
				puts(Relations.areFriends(x, y) ? "1" : "0");
				break;
			case 4:
				puts(Relations.areEnemies(x, y) ? "1" : "0");
				break;
		}
	}

	return 0;
}
