#include <iostream>
#include <vector>
#include <cstring>
#include <climits>


using namespace std;

int solution[200][20];
int CValue[20][20];

int shop(int m, int g, const int C, const int M){
	if(m < 0) return INT_MIN;
	if(g == C) return M - m;
	if(solution[m][g] != -1) return solution[m][g];

	int max = INT_MIN;
	for(int i = 0; CValue[g][i] != 0; i++){
		int vShop = shop(m - CValue[g][i], g + 1, C, M);
		max = max < vShop ? vShop : max;
	}

	return solution[m][g] = max;
}

int main(void){
	int N;

	cin >> N;

	while(N--){
		memset(solution, -1, sizeof(solution));
		memset(CValue, 0, sizeof(CValue));

		int M, C;
		cin >> M >> C;

		for(int i = 0; i < C; i++){
			int item_i;
			for(cin >> item_i; item_i > 0; item_i--)
				cin >> CValue[i][item_i - 1];
		}

		int ans = shop(M, 0, C, M);

		if(ans > 0)
			cout << ans << endl;
		else
			cout << "no solution" << endl;
	}

	return 0;
}