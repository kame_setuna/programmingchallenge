#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <cstring>

using namespace std;

int M, N;
int dp[10200][101];
vector<pair<int, int>> clothes;

int shopping(int m, int n){
	if(m > M  && M <= 1800) return -2000;
	if(m > M + 200) return -2000;
	if(n == clothes.size()){
		if(m > M && m <= 2000) 
			return -2000;
		return 0;
	}

	if(dp[m][n] != -1) return dp[m][n];

	
	return dp[m][n] = max(shopping(m, n + 1),
		clothes[n].second + shopping(m + clothes[n].first, n + 1));
}

int main(void){
	while(cin >> M >> N){
		memset(dp, -1, sizeof(dp));
		int price, index;
		for(int i = 0; i < N; i++){
			cin >> price >> index;
			clothes.push_back({price, index});
		}
		int ans;
		ans = shopping(0, 0);
		cout << ans << endl;

		clothes.clear();
	}

	return 0;
}