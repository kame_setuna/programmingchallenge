#include <iostream>
#include <cstring>

using namespace std;

int Stops[20000];

int nicestRoute(int& begin, int& end){
	int sum = 0, ans = 0;
	int from, to;
	from = 1, to = 2;

	for(int i = 1; i < Stops[0]; i++){
		sum += Stops[i];

		if(ans < sum){
			ans = sum;
			to = i + 1;
			begin = from;
			end = to;
		} else if(ans == sum){
			to = i + 1;
			if(end - begin < to - from){
				begin = from;
				end = to;
			}
		}

		if(sum < 0){
			sum = 0;
			from = i + 1;
		}
	}

	return ans;
}

int main(void){
	int B;

	cin >> B;

	for(int route = 1; route <= B; route++){
		memset(Stops, 0, sizeof(Stops));

		cin >> Stops[0];
		for(int i = 1; i < Stops[0]; i++)
			cin >> Stops[i];
		int begin, end;

		if(nicestRoute(begin, end) == 0){
			cout << "Route " << route << " has no nice parts" << endl;
		} else
			cout << "The nicest part of route " << route << " is between stops "
			<< begin << " and " << end << endl;
	}

	return 0;
}