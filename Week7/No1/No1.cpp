#include <iostream>
#include <utility>
#include <set>
#include <functional>
#include <cmath>

using namespace std;

typedef set<pair<int, int>, greater<pair<int, int>>> IntSetReverse;

int main(void){
	int c;

	cin >> c;

	while(c--){
		IntSetReverse coordinates;
		int N;
		cin >> N;
		int x, y;
		for(int i = 0; i < N; i++){
			cin >> x >> y;
			coordinates.insert({x,y});
		}

		IntSetReverse::iterator it, itp;
		int altitude = 0;
		double sunny = 0;

		for(it = coordinates.begin(); it != coordinates.end(); itp = it, it++){
			if((*it).second > altitude){
				double l, l_;
				double delta_y, delta_x;
				delta_x = (*it).first - (*itp).first;
				delta_y = (*it).second - (*itp).second;
				l = sqrt(pow(delta_x, 2) + pow(delta_y, 2));

				l_ = l*((*it).second - altitude) / delta_y;
				sunny += l_;
				altitude = (*it).second;
				
			}
		}
	
		printf("%.2lf\n", sunny);
		
	}
}