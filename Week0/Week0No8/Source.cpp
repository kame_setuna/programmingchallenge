#include <iostream>
#include <string>

using namespace std;

void printMap(int map[20][20]) {
	for(int i = 0; i < 20; i++) {
		for(int j = 0; j < 20; j++)
			if(map[i][j] == 0)
				cout << ' ';
			else
				cout << 'O';
		cout << '\n';
	}

}

int countNeighbors(int map[20][20], int NS, int EW) {
	int cnt = 0;

	if(NS > 0) {
		if(EW > 0 && map[NS - 1][EW - 1] == 1)
			cnt++;
		if(map[NS - 1][EW] == 1)
			cnt++;
		if(EW < 19 && map[NS - 1][EW + 1] == 1)
			cnt++;
	}

	if(EW > 0 && map[NS][EW - 1] == 1)
		cnt++;
	if(EW < 19 && map[NS][EW + 1] == 1)
		cnt++;

	if(NS < 19) {
		if(EW > 0 && map[NS + 1][EW - 1] == 1)
			cnt++;
		if(map[NS + 1][EW] == 1)
			cnt++;
		if(EW < 19 && map[NS + 1][EW + 1] == 1)
			cnt++;
	}

	return cnt;
}

int main(void) {
	string tmp;
	int Cases, Years;
	const char asterisk[22] = "********************\n";

	cin >> Cases;

	while(Cases--) {
		cin >> Years;

		int mapOld[20][20];// 0: Quarters don't exit
		int mapNew[20][20];// 1: Quarters exit

		for(int i = 0; i < 20; i++)
			for(int j = 0; j < 20; j++)
				mapOld[i][j] = 0;

		{
			int NS, EW;
			cin.ignore();
			while(getline(cin, tmp), tmp != "") {
				sscanf(tmp.c_str(), " %d %d", &NS, &EW);
				mapOld[NS - 1][EW - 1] = 1;
			}
		}

		

		while(Years--) {
			cout << asterisk;
			printMap(mapOld);
			for(int i = 0; i < 20; i++) {
				for(int j = 0; j < 20; j++) {
					switch(countNeighbors(mapOld, i, j)) {
						case 0:
						case 1:
							mapNew[i][j] = 0;
							break;
						case 2:
							if(mapOld[i][j] == 0)
								mapNew[i][j] = 0;
							else
								mapNew[i][j] = 1;
							break;
						case 3:
							mapNew[i][j] = 1;
							break;
						case 4:
						case 5:
						case 6:
						case 7:
						case 8:
							mapNew[i][j] = 0;
							break;
					}
				}
			}

			for(int i = 0; i < 20; i++)
				for(int j = 0; j < 20; j++)
					mapOld[i][j] = mapNew[i][j];
		}

		cout << asterisk;

		if(Cases == 0)
			;
		else
			cout << '\n';
	}

	return 0;
}