#include <iostream>
#include <string>

using namespace std;

string determineSuit(int i) {
	switch(i / 13) {
		case 0:
			return "Clubs";
		case 1:
			return "Diamonds";
		case 2:
			return "Hearts";
		case 3:
			return "Spades";
	}
}

string determineValue(int i) {
	switch(i % 13) {
		case 0:
			return "2";
		case 1:
			return "3";			
		case 2:
			return "4";			
		case 3:
			return "5";			
		case 4:
			return "6";			
		case 5:
			return "7";			
		case 6:
			return "8";			
		case 7:
			return "9";			
		case 8:
			return "10";			
		case 9:
			return "Jack";			
		case 10:
			return "Queen";			
		case 11:
			return "King";			
		case 12:
			return "Ace";			
	}
}

int main(void) {
	int numOfcase;
	int numOfway;
	int wayUsed;
	int Card[52];
	int tmp;
	string Suit, Value, s;

	cin >> numOfcase;

	while(numOfcase--) {
		cin >> numOfway;
		int *Shuffle = new int[numOfway * 52];

		for(int i = 0; i < 52; i++)
			Card[i] = i;

		for(int i = 0; i < numOfway; i++)
			for(int j = 0; j < 52; j++) {
				cin >> tmp;
				Shuffle[i * 52 + j] = tmp - 1;
			}

		cin.ignore();
		getline(cin, s);
		while(s != "") {
			wayUsed = atoi(s.c_str());
			int CardBuf[52];
			for(int i = 0; i < 52; i++) 
				CardBuf[i] = Card[Shuffle[(wayUsed-1) * 52 + i]];
			for(int i = 0; i < 52; i++)
				Card[i] = CardBuf[i];

			getline(cin, s);
		}

		for(int i = 0; i < 52; i++) {
			Suit = determineSuit(Card[i]);
			Value = determineValue(Card[i]);
			cout << Value << " of " << Suit << endl;
		}
		if(numOfcase != 0)
			cout << "\n";
	}

	return 0;
}