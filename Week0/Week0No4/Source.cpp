#include <iostream>

using namespace std;

int main(void) {
	int T, N;
	int minSpeed, SpeedOfcrt;

	cin >> T;

	for(int i = 0; i < T; i++) {
		minSpeed = 0;
		cin >> N;
		for(int j = 0; j < N; j++) {
			cin >> SpeedOfcrt;
			if(SpeedOfcrt > minSpeed)
				minSpeed = SpeedOfcrt;
		}

		cout << "Case " << i + 1 << ": " << minSpeed << endl;
	}

	return 0;
}