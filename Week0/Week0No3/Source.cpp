#include <iostream>

using namespace std;

int main(void) {
	int N, B, H, W;
	int price_Hotel;
	int LeastBudget;
	int availableBeds;

	while(cin >> N >> B >> H >> W) {
		LeastBudget = B + 1;
		for(int i = 0; i < H; i++) {
			cin >> price_Hotel;

			for(int j = 0; j < W; j++) {
				cin >> availableBeds;
				if(availableBeds >= N
					&& N*price_Hotel < LeastBudget)
					LeastBudget = N*price_Hotel;
			}
		}

		if(LeastBudget > B)
			cout << "stay home\n";
		else
			cout << LeastBudget << endl;
	}

	return 0;
}