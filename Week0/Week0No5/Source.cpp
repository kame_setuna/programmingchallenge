#include <iostream>

using namespace std;

const

bool isLeap(int Year) {
	if(Year % 400 == 0)
		return true;
	if(Year % 4 == 0 && Year % 100 != 0)
		return true;

	return  false;
}

int DateOfMonth(int Month, int Year) {
	int _DateOfMonth[13] = { 0, 31,28,31,30,31,30,31,31,30,31,30,31 };

	if(Month == 2 && isLeap(Year)) {
		return 29;
	} else
		return _DateOfMonth[Month];
}

int DateOfYear(int Year) {
	return isLeap(Year) ? 366 : 365;
}

int daysFrom1thJan(int Year, int Month, int Date) {
	int Days = 0;

	for(int i = 1; i < Month; i++)
		Days += DateOfMonth(i, Year);

	Days += (Date - 1);

	return Days;
}

int main(void) {
	int Days, Date, Month, Year, YearCount;

	while(1) {
		cin >> Days >> Date >> Month >> Year;
		if(0 == Days && 0 == Date
			&& 0 == Month && 0 == Year)
			return 0;

		Days += daysFrom1thJan(Year, Month, Date);
		Month = Date = 1;

		while(Days >= DateOfYear(Year)) {
			if(isLeap(Year)) {
				Year++;
				Days -= 366;
			} else {
				Year++;
				Days -= 365;
			}
		}

		for(int i = 1; i <= 12; i++) {
			if(Days < DateOfMonth(i, Year))
				break;

			Month++;
			Days -= DateOfMonth(i, Year);
		}

		Date += Days;

		cout << Date << " " << Month << " " << Year << endl;
	}

	return 0;
}