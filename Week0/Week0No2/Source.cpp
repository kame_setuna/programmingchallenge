#include <iostream>

using namespace std;

int main(void) {
	int T, a, b, c;

	cin >> T;

	for (int i = 0; i < T; i++) {
		cin >> a >> b >> c;
		if (a > b) swap(a, b);
		if (b > c) swap(b, c);
		if (a > b) swap(a, b);
		cout << "Case " << i + 1 << ": " << b << endl;
	}

	return 0;
}
