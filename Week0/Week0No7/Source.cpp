#include <iostream>

using namespace std;

int main(void) {
	int tmp, i, j;
	int Signals[100];
	bool AllGreen = false;

	while(1) {
		cin >> tmp;
		if(tmp == 0)
			return 0;

		for(i = 0; tmp != 0; cin >> tmp, i++)
			Signals[i] = tmp;

		int minIdx = 0;
		for(int j = 1; j < i; j++)
			if(Signals[j] < Signals[minIdx])
				minIdx = j;

		int Sec = Signals[minIdx] * 2;

		for(j = 0; j <= 5 * 60 * 60 - Sec; j++) {
			AllGreen = true;
			for(int k = 0; k < i; k++)
				if(!(((Sec + j) % (Signals[k] * 2)) < (Signals[k] - 5))) {
					AllGreen = false;
					break;
				}
			if(AllGreen) {
				Sec += j;
				break;
			}
		}

		if(j == 5 * 3600 - Sec + 1) {
			cout << "Signals fail to synchronise in 5 hours\n";
			continue;
		}

		const int H = Sec / 3600;
		Sec %= 3600;
		const int M = Sec / 60;
		Sec %= 60;
		printf("%02d:%02d:%02d\n", H, M, Sec);
	}

	return 0;
}