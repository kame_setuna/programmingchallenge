#include <iostream>

using namespace std;

int ThreeNplusOne(int x) {
	if (x == 1)	return 1;

	if (x % 2 == 0)
		x /= 2;
	else
		x = 3 * x + 1;

	return 1 + ThreeNplusOne(x);
}

int main(void) {
	int i, j;
	int max = 0;/*For maximum Cycle*/
	int tmp;/*For temporary*/
	
	while (cin >> i >> j) {
		max = 0;
		cout << i << ' ' << j << ' ';

		if (i > j) {
			tmp = i;
			i = j;
			j = tmp;
		}

		for (; i <= j; i++) {
			tmp = ThreeNplusOne(i);
			if (tmp > max)
				max = tmp;
		}

		cout << max << endl;
		
	}

	return 0;
}